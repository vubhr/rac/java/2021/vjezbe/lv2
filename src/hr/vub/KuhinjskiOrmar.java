package hr.vub;

public class KuhinjskiOrmar {
   private int Čačkalice;
   private int Čaša;
   private int Šalica;
   private int Đem;

    public KuhinjskiOrmar(int čačkalice, int čaša, int šalica, int đem) {
        Čačkalice = čačkalice;
        Čaša = čaša;
        Šalica = šalica;
        Đem = đem;
    }


    public int getČačkalice() {
        return Čačkalice;
    }

    public void setČačkalice(int čačkalice) {
        Čačkalice = čačkalice;
    }

    public int getČaša() {
        return Čaša;
    }

    public void setČaša(int čaša) {
        Čaša = čaša;
    }

    public int getŠalica() {
        return Šalica;
    }

    public void setŠalica(int šalica) {
        Šalica = šalica;
    }

    public int getĐem() {
        return Đem;
    }

    public void setĐem(int đem) {
        Đem = đem;
    }

    @Override
    public String toString() {
        return "hr.vub.KuhinjskiOrmar{" +
                "Čačkalice=" + Čačkalice +
                ", Čaša=" + Čaša +
                ", Šalica=" + Šalica +
                ", Đem=" + Đem +
                '}';
    }
}
